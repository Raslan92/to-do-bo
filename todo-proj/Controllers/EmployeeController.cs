using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using TodoApi.Models;

namespace TodoApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class EmployeeController:ControllerBase
    {
       
        private readonly TodoApiDbContext _context;
        public EmployeeController(TodoApiDbContext context)
        {
            _context=context;
            
        }
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Employee>>>GetEmployees(){
            return await  _context.Mohamed_Employees.ToListAsync();
        }

        [HttpGet("{id}")]
         public async Task<ActionResult<Employee>> GetEmployee(int id)
        {
            var employee = await _context.Mohamed_Employees.FindAsync(id);
            if(employee == null){
                return NotFound();
            }

            return employee;
        }

        //Post:api/Employee
        [HttpPost]
        public async Task<ActionResult<Employee>> PostEmployee(Employee employee)
        {
            _context.Mohamed_Employees.Add(employee);
            await _context.SaveChangesAsync();

            //return CreatedAtAction("GetEmployee", new { id = todoItem.Id }, todoItem);
            return CreatedAtAction(nameof(GetEmployee), new { id = employee.Id }, employee);
        }
        //put : api/employee
        [HttpPut("{id}")]
        public async Task<IActionResult> PutEmployeee(int id, Employee employee)
        {
            var eId= await _context.Mohamed_Employees.FindAsync(id);
            if (eId==null)
            {
                return BadRequest();
            }

            _context.Entry(employee).State = EntityState.Modified;

            try
            {
                _context.Mohamed_Employees.Update(employee);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EmployeeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }
        //delete 
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteEmployee(int id)
        {
            var employee = await _context.Mohamed_Employees.FindAsync(id);
            if (employee == null)
            {
                return NotFound();
            }

            _context.Mohamed_Employees.Remove(employee);
            await _context.SaveChangesAsync();

            return NoContent();
        }
         private bool EmployeeExists(int id)
        {
            return _context.Mohamed_Employees.Any(e => e.Id == id);
        }

    }
}