﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TodoApi.Migrations
{
    public partial class third : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Employees_Departments_DepartmentDeptCode",
                table: "Employees");

            migrationBuilder.AlterColumn<int>(
                name: "DepartmentDeptCode",
                table: "Employees",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Employees_Departments_DepartmentDeptCode",
                table: "Employees",
                column: "DepartmentDeptCode",
                principalTable: "Departments",
                principalColumn: "DeptCode",
                onDelete: ReferentialAction.SetNull);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Employees_Departments_DepartmentDeptCode",
                table: "Employees");

            migrationBuilder.AlterColumn<int>(
                name: "DepartmentDeptCode",
                table: "Employees",
                type: "integer",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_Employees_Departments_DepartmentDeptCode",
                table: "Employees",
                column: "DepartmentDeptCode",
                principalTable: "Departments",
                principalColumn: "DeptCode",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
