﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TodoApi.Migrations
{
    public partial class sixth : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Employees_Departments_DeptCode",
                table: "Employees");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Employees",
                table: "Employees");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Departments",
                table: "Departments");

            migrationBuilder.RenameTable(
                name: "Employees",
                newName: "Mohamed_Employees");

            migrationBuilder.RenameTable(
                name: "Departments",
                newName: "Mohamed_Departments");

            migrationBuilder.RenameIndex(
                name: "IX_Employees_DeptCode",
                table: "Mohamed_Employees",
                newName: "IX_Mohamed_Employees_DeptCode");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Mohamed_Employees",
                table: "Mohamed_Employees",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Mohamed_Departments",
                table: "Mohamed_Departments",
                column: "DeptCode");

            migrationBuilder.AddForeignKey(
                name: "FK_Mohamed_Employees_Mohamed_Departments_DeptCode",
                table: "Mohamed_Employees",
                column: "DeptCode",
                principalTable: "Mohamed_Departments",
                principalColumn: "DeptCode",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Mohamed_Employees_Mohamed_Departments_DeptCode",
                table: "Mohamed_Employees");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Mohamed_Employees",
                table: "Mohamed_Employees");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Mohamed_Departments",
                table: "Mohamed_Departments");

            migrationBuilder.RenameTable(
                name: "Mohamed_Employees",
                newName: "Employees");

            migrationBuilder.RenameTable(
                name: "Mohamed_Departments",
                newName: "Departments");

            migrationBuilder.RenameIndex(
                name: "IX_Mohamed_Employees_DeptCode",
                table: "Employees",
                newName: "IX_Employees_DeptCode");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Employees",
                table: "Employees",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Departments",
                table: "Departments",
                column: "DeptCode");

            migrationBuilder.AddForeignKey(
                name: "FK_Employees_Departments_DeptCode",
                table: "Employees",
                column: "DeptCode",
                principalTable: "Departments",
                principalColumn: "DeptCode",
                onDelete: ReferentialAction.SetNull);
        }
    }
}
