using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TodoApi.Models
{
    public class Employee
    {
        [Key]
        public int Id{get;set;}

        [Column(TypeName="varchar(100)")]
        public string Name{get;set;}

        [Column(TypeName="varchar(100)")]
        public string email{get;set;}

        [Column(TypeName="varchar(100)")]
        public string ImageUrl{get;set;}

        [ForeignKey("Department")]
        public int DeptCode{get;set;}
        public Department Department{get;set;}
    }
}